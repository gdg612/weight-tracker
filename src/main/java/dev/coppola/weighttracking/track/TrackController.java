package dev.coppola.weighttracking.track;

import dev.coppola.weighttracking.security.TokenAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TrackController {

    private static final String WEIGHT_RECORDS = "weight_records";

    @Autowired
    TrackRepository trackRepository;

    /**
     * This action is the default page that shows all the tracks, the form to add a new one and a chart.
     *
     * @param model
     * @return
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/track")
    protected String home(final Model model, final Authentication authentication) {
        logger.info("Home page");

        if (authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            model.addAttribute("profile", tokenAuthentication.getClaims());
        }

        return "index";
    }

    /**
     * This action is "Add new weight track", activated by the form send.
     *
     * @param weightValue
     * @param model
     * @param session
     * @return
     */
    @PostMapping("/track")
    public void Track(
            @RequestParam(name = "weightValue", defaultValue = "0.0f") Double weightValue,
            Model model,
            HttpSession session,
            HttpServletResponse response) throws IOException {

        List<Track> records = getTracksFromSession(session);

        Track w = new Track(LocalDate.now(ZoneId.systemDefault()), "KG", weightValue);
        records.add(w);

        session.setAttribute(WEIGHT_RECORDS, records);

        response.sendRedirect("");
    }


    @PostMapping("/track/save")
    public void saveTrack(HttpServletResponse response,
                          HttpSession session,
                          final Authentication authentication,
                          RedirectAttributes redirectAttributes) throws Exception{
        if (authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            String userMail = tokenAuthentication.getClaims().get("email").asString();
            List<Track> records = getTracksFromSession(session);

            for( Track t : records){
                t.setUserMail(userMail);
            }

            trackRepository.saveAll(records);
        }
        redirectAttributes.addFlashAttribute("saved", true);
        response.sendRedirect("");
    }

    private List<Track> getTracksFromSession(HttpSession session) {
        List<Track> records = (ArrayList<Track>) session.getAttribute(WEIGHT_RECORDS);

        if (records == null) {
            records = new ArrayList<Track>();
        }
        return records;
    }

}
