package dev.coppola.weighttracking.track;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Entity
@Table(name = "tracks")
@Data
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String userMail;
    private LocalDate trackedDate;
    private String unit;
    private Double weight;

    public Track(LocalDate trackedDate, String unit, Double weight){
        this.trackedDate = trackedDate;
        this.unit = unit;
        this.weight = weight;
    }

    public Track(String userMail, LocalDate trackedDate, String unit, Double weight) {
        this(trackedDate, unit, weight);
        this.userMail = userMail;
    }
}
