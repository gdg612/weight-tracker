(function () {
    var options = {
        maintainAspectRatio: false,
        spanGaps: false,
        elements: {
            line: {
                tension: 0.000001
            }
        },
        plugins: {
            filler: {
                propagate: false
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false,
                    maxRotation: 0
                }
            }]
        }
    };

    const labels = dataSet1.map(e => e.trackedDate);
    const data = dataSet1.map(e => e.weight);

    new Chart('weightChart', {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                label: 'Tracks',
                fill: true
            }]
        },
        options: Chart.helpers.merge(options, {
            title: {
                text: 'fill: measures',
                display: true
            }
        })
    });
})();